#!/bin/sh
# Script that sets a version as first argument and releases it to Nexus.
# !!! Before release, prepare your settings.xml (with server id=ossrh and profile id=ossrh with gpg)

mvn versions:set -DnewVersion="$1"
mvn clean install deploy -P release
