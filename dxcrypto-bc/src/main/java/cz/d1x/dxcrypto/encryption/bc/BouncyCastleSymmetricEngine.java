package cz.d1x.dxcrypto.encryption.bc;

import cz.d1x.dxcrypto.encryption.EncryptionException;
import cz.d1x.dxcrypto.encryption.StreamingEncryptionEngine;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.io.CipherInputStream;
import org.bouncycastle.crypto.io.CipherOutputStream;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.BlockCipherPadding;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implementation of encryption engine that uses Bouncy Castle implementations for symmetric encryption.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public class BouncyCastleSymmetricEngine implements StreamingEncryptionEngine {

    private final Class<? extends BlockCipher> blockCipherClass;
    private final KeyParameter keyParam;

    public BouncyCastleSymmetricEngine(Class<? extends BlockCipher> blockCipherClass, KeyParameter keyParam) {
        this.blockCipherClass = blockCipherClass;
        this.keyParam = keyParam;
    }

    @Override
    public byte[] encrypt(byte[] input, byte[] initVector) throws EncryptionException {
        return doOperation(input, initVector, true);
    }

    @Override
    public byte[] decrypt(byte[] input, byte[] initVector) throws EncryptionException {
        return doOperation(input, initVector, false);
    }

    @Override
    public InputStream encrypt(InputStream input, byte[] initVector) throws EncryptionException {
        BufferedBlockCipher cipher = createCipher(initVector, true);
        return new CipherInputStream(input, cipher);
    }

    @Override
    public OutputStream encrypt(OutputStream output, byte[] initVector) {
        BufferedBlockCipher cipher = createCipher(initVector, true);
        return new CipherOutputStream(output, cipher);
    }

    @Override
    public InputStream decrypt(InputStream input, byte[] initVector) {
        BufferedBlockCipher cipher = createCipher(initVector, false);
        return new CipherInputStream(input, cipher);
    }

    @Override
    public OutputStream decrypt(OutputStream output, byte[] initVector) throws EncryptionException {
        BufferedBlockCipher cipher = createCipher(initVector, false);
        return new CipherOutputStream(output, cipher);
    }

    private BufferedBlockCipher createCipher(byte[] initVector, boolean isEncrypt) {
        CipherParameters params = new ParametersWithIV(keyParam, initVector);
        BlockCipherPadding padding = new PKCS7Padding();
        BlockCipher blockCipher;
        try {
            blockCipher = blockCipherClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new EncryptionException("Encryption fails", e);
        }
        BlockCipher engine = new CBCBlockCipher(blockCipher);
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(engine, padding);
        cipher.init(isEncrypt, params);
        return cipher;
    }

    private byte[] doOperation(byte[] input, byte[] initVector, boolean isEncrypt) {
        BufferedBlockCipher cipher = createCipher(initVector, isEncrypt);

        byte[] output = new byte[cipher.getOutputSize(input.length)];
        int length = cipher.processBytes(input, 0, input.length, output, 0);
        try {
            length += cipher.doFinal(output, length);
        } catch (InvalidCipherTextException e) {
            throw new EncryptionException("Encryption fails", e);
        }

        // Remove output padding
        byte[] out = new byte[length];
        System.arraycopy(output, 0, out, 0, length);
        return out;
    }

}
