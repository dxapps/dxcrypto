package cz.d1x.dxcrypto.hash.bc;

import cz.d1x.dxcrypto.common.BytesRepresentation;
import cz.d1x.dxcrypto.common.Encoding;
import cz.d1x.dxcrypto.hash.HashingException;
import cz.d1x.dxcrypto.hash.SaltedHashingAlgorithm;
import org.bouncycastle.crypto.generators.BCrypt;

public class BouncyCastleBCrypt implements SaltedHashingAlgorithm {

    private static final int BCRYPT_COST = 10; // reasonable value for 2020 when it comes to time
    private final BytesRepresentation bytesRepresentation;
    private final String encoding;

    protected BouncyCastleBCrypt(BytesRepresentation bytesRepresentation, String encoding) {
        this.bytesRepresentation = bytesRepresentation;
        this.encoding = encoding;
    }

    @Override
    public byte[] hash(byte[] input, byte[] salt) throws HashingException {
        if (input == null || salt == null) {
            throw new IllegalArgumentException("Input data for hashing cannot be null");
        }
        return BCrypt.generate(input, salt, BCRYPT_COST);
    }

    @Override
    public String hash(String input, String salt) throws HashingException {
        if (input == null) {
            throw new IllegalArgumentException("Input data for hashing cannot be null");
        }
        byte[] textBytes = Encoding.getBytes(input, encoding);
        byte[] saltBytes = Encoding.getBytes(salt, encoding);
        byte[] hash = hash(textBytes, saltBytes);
        return bytesRepresentation.toString(hash);
    }
}
