package cz.d1x.dxcrypto.hash.bc;

import cz.d1x.dxcrypto.hash.HashingAlgorithms;
import cz.d1x.dxcrypto.hash.NativeSaltedHashingAlgorithmBuilder;
import cz.d1x.dxcrypto.hash.SaltedHashingAlgorithm;

/**
 * Builder that builds {@link BouncyCastleBCrypt} instances.
 * You should use {@link BouncyCastleHashingAlgorithms} factory for creating instances.
 *
 * @author Zdenek Obst, zdenek.obst-at-gmail.com
 */
public final class BouncyCastleBcryptBuilder extends NativeSaltedHashingAlgorithmBuilder {

    @Override
    public SaltedHashingAlgorithm build() {
        return new BouncyCastleBCrypt(bytesRepresentation, encoding);
    }
}
