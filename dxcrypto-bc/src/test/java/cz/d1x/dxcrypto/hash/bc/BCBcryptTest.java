package cz.d1x.dxcrypto.hash.bc;

import cz.d1x.dxcrypto.hash.SaltedHashingAlgorithm;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BCBcryptTest {

    private SaltedHashingAlgorithm algorithm;

    @Before
    public void setUp() {
        this.algorithm = BouncyCastleHashingAlgorithms.bcrypt()
                .build();
    }

    /**
     * Tests that null for hashing throws exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void nullHashing() {
        algorithm.hash((String) null, (String) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyStringThrowsException() {
        algorithm.hash("", "");
    }

    @Test
    public void simpleStringIsHashedCorrectly() {
        String salt = "0123456789abcdef";// BCrypt requires 128bits (16 bytes) size of salt
        String output = algorithm.hash("Toto-jePrvni.vstupPro_h@sh", salt);

        assertEquals("4027b0a63f667c63987213034d76de447d1700b72ec78bf4", output);
    }

    @Test
    public void binaryStringIsHashedCorrectly() {
        String salt = "0123456789abcdef";// BCrypt requires 128bits (16 bytes) size of salt
        String output = algorithm.hash("00101010101111010101011110100101", salt);

        assertEquals("c8ee0fd3449be415f0f0e049365d95e7bdbf10b37ed68cc5", output);
    }
}
