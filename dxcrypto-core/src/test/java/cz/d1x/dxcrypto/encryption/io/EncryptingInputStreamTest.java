package cz.d1x.dxcrypto.encryption.io;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Test for {@link EncryptingInputStream}
 * Tests correct reading of IV from given InputStream.
 *
 * @author d.richter
 */
public class EncryptingInputStreamTest {

    @Test
    public void ivAndDataAreReadedCorrectly() throws IOException {
        final EncryptingInputStream input = new EncryptingInputStream(new ByteArrayInputStream(new byte[] {0, 1, 2, 3, 4, 5}), new byte[] {10, 11, 12, 13});
        final byte[] buffer = new byte[10];
        Assert.assertEquals(10, IOUtils.read(input, buffer));
        Assert.assertArrayEquals(new byte[] {10, 11, 12, 13, 0, 1, 2, 3, 4, 5}, buffer);
        Assert.assertEquals(-1, input.read());
    }

    @Test
    public void availableReturnsCorrectValues() throws IOException {
        final EncryptingInputStream input = new EncryptingInputStream(new ByteArrayInputStream(new byte[] {0, 1, 2, 3, 4, 5}), new byte[] {10, 11, 12, 13});
        Assert.assertEquals(10, input.available());
        //noinspection ResultOfMethodCallIgnored
        input.read();
        Assert.assertEquals(9, input.available());
    }

    @Test
    public void markSupportedReturnsFalse() {
        final EncryptingInputStream input = new EncryptingInputStream(new ByteArrayInputStream(new byte[] {0, 1, 2, 3, 4, 5}), new byte[] {10, 11, 12, 13});
        Assert.assertFalse(input.markSupported());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void skipThrowsUnsupportedOperationException() throws IOException {
        final EncryptingInputStream input = new EncryptingInputStream(new ByteArrayInputStream(new byte[] {0, 1, 2, 3, 4, 5}), new byte[] {10, 11, 12, 13});
        //noinspection ResultOfMethodCallIgnored
        input.skip(10);
    }
}