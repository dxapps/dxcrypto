package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;

/**
 * Interface for all test that are related to specific list of {@link EncryptionAlgorithm}
 * implementations.
 */
public interface EncryptionTest {

    /**
     * Tests given {@link EncryptionAlgorithm}.
     *
     * @param algorithm algorithm to be tested
     */
    TestResult test(EncryptionAlgorithm algorithm);
}
