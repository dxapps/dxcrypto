package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;
import cz.d1x.dxcrypto.encryption.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Simple test that encryption and decryption of stream works correctly
 */
public class EncryptedOutputStreamCanBeCorrectlyDecrypted implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        String plainString = "Som3 T@";
        if (!(algorithm instanceof StreamingEncryptionAlgorithm)) {
            return TestResult.success();
        }

        final StreamingEncryptionAlgorithm streamingAlgorithm = (StreamingEncryptionAlgorithm) algorithm;

        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        try (OutputStream encrypted = streamingAlgorithm.encrypt(output)) {
            encrypted.write(plainString.getBytes(UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
            return TestResult.fail(e.getMessage());
        }
        final byte[] encryptedBytes = output.toByteArray();
        if (new String(encryptedBytes, UTF_8).contains(plainString)) {
            return TestResult.fail("Encrypted string contains original string");
        }

        final String decryptedString;
        try (InputStream decrypted = streamingAlgorithm.decrypt(new ByteArrayInputStream(encryptedBytes))) {
            byte[] buffer = new byte[plainString.getBytes(UTF_8).length + 2];//add some more trailing size to check if there are some more bytes in stream after decrypt
            final int read = IOUtils.read(decrypted, buffer);
            decryptedString = new String(Arrays.copyOf(buffer, read), UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return TestResult.fail(e.getMessage());
        }
        final ByteArrayOutputStream decryptedStream = new ByteArrayOutputStream();
        try (OutputStream decrypted = streamingAlgorithm.decrypt(decryptedStream)) {
            decrypted.write(encryptedBytes);
        } catch (IOException e) {
            e.printStackTrace();
            return TestResult.fail(e.getMessage());
        }
        if (!new String(decryptedStream.toByteArray(), UTF_8).equals(plainString)) {
            return TestResult.fail("Original and decrypted strings does not match");
        }
        if (!decryptedString.equals(plainString)) {
            return TestResult.fail("Original and decrypted strings does not match");
        }
        return TestResult.success();
    }
}
