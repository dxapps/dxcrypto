package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Tests that simple encrypted input can be decrypted correctly back to original value.
 */
public class EncryptedInputCanBeCorrectlyDecrypted implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        String plain = "th1s_is_something inter3sting -*";
        byte[] plainBytes;
        try {
            plainBytes = plain.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return TestResult.fail("Unable to get bytes of UTF-8");
        }
        byte[] encryptedBytes = algorithm.encrypt(plainBytes);
        byte[] decryptedBytes = algorithm.decrypt(encryptedBytes);
        if (Arrays.equals(encryptedBytes, decryptedBytes))
            return TestResult.fail("Encrypted and decrypted bytes are the same");
        if (!Arrays.equals(plainBytes, decryptedBytes))
            return TestResult.fail("Original and decrypted strings are not equal");


        String encrypted = algorithm.encrypt(plain);
        String decrypted = algorithm.decrypt(encrypted);
        if (encrypted.equals(decrypted))
            return TestResult.fail("Encrypted and decrypted bytes are the same");
        if (!plain.equals(decrypted))
            return TestResult.fail("Original and decrypted strings are not equal");

        return TestResult.success();
    }

}
