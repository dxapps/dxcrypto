package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;

/**
 * Tests that two same inputs will have different output (thanks to different IV).
 */
public class SameInputsHaveDifferentOutputForSymmetric implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        String plain = "th1s_is_something inter3sting -*";
        if (!(algorithm instanceof GenericEncryptionAlgorithm)) {
            return TestResult.success();
        }
        String encrypted1 = algorithm.encrypt(plain);
        String encrypted2 = algorithm.encrypt(plain);
        if (encrypted1.equals(encrypted2)) return TestResult.fail("Outputs should be different");

        String encrypted1End = encrypted1.substring(40);
        String encrypted2End = encrypted2.substring(40);
        if (encrypted1End.equals(encrypted2End)) return TestResult.fail("Output ends should be different");

        return TestResult.success();
    }
}
