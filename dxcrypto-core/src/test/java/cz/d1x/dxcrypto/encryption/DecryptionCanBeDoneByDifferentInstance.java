package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;

/**
 * Tests that decryption can be made by different instance than the one used for encryption.
 */
public class DecryptionCanBeDoneByDifferentInstance implements EncryptionTest {

    @Override
    public TestResult test(EncryptionAlgorithm algorithm) {
        return null;
    }
}
