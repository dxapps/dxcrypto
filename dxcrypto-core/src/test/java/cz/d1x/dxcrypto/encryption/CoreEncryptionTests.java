package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.encryption.crypto.CryptoFactories;
import cz.d1x.dxcrypto.encryption.key.RSAKeyParams;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Tests core implementations against base set of encryption tests.
 */
public class CoreEncryptionTests extends BaseEncryptionTests {

    private static final byte[] AES_KEY = {
            0x27, 0x18, 0x27, 0x09, 0x7C, 0x44, 0x17, 0x1E,
            0x43, 0x03, 0x11, 0x27, 0x1F, 0x0D, 0x6D, 0x64};
    private static final byte[] TRIPLE_DES_KEY = {
            0x27, 0x18, 0x27, 0x09, 0x7C, 0x44, 0x17, 0x1E,
            0x43, 0x03, 0x11, 0x27, 0x1F, 0x0D, 0x6D, 0x64,
            0x44, 0x18, 0x27, 0x09, 0x7A, 0x44, 0x17, 0x3E};
    private static final RSAKeyParams[] RSA_KEYS = new RSAKeysGenerator().generateKeys();

    @BeforeClass
    public static void setupFactories() {
        EncryptionAlgorithms.defaultFactories(new CryptoFactories());
    }

    @Override
    protected Collection<EncryptionAlgorithm> getAlgorithmsToTest() {
        return new ArrayList<EncryptionAlgorithm>() {{
            add(EncryptionAlgorithms.aes(AES_KEY).build());
            add(EncryptionAlgorithms.tripleDes(TRIPLE_DES_KEY).build());

            add(EncryptionAlgorithms.rsa()
                    .publicKey(RSA_KEYS[0].getModulus(), RSA_KEYS[0].getExponent())
                    .privateKey(RSA_KEYS[1].getModulus(), RSA_KEYS[1].getExponent())
                    .build());
        }};
    }

    @Test
    @Override
    public void nullInputsThrowEncryptionException() {
        super.nullInputsThrowEncryptionException();
    }

    @Test
    @Override
    public void encryptedInputCanBeCorrectlyDecrypted() {
        super.encryptedInputCanBeCorrectlyDecrypted();
    }

    @Test
    @Override
    public void byteAndStringBasedMethodsGiveSameOutput() {
        super.byteAndStringBasedMethodsGiveSameOutput();
    }

    @Test
    @Override
    public void sameInputsHaveDifferentOutputsForSymmetricAlgorithms() {
        super.sameInputsHaveDifferentOutputsForSymmetricAlgorithms();
    }

    @Test
    @Override
    public void encryptedInputStreamCanBeCorrectlyDecrypted() {
        super.encryptedInputStreamCanBeCorrectlyDecrypted();
    }

    @Test
    @Override
    public void encryptedOutputStreamCanBeCorrectlyDecrypted() {
        super.encryptedOutputStreamCanBeCorrectlyDecrypted();
    }

    @Test
    @Override
    public void concurrentEncryptingThreadsDoesNotInterfere() {
        super.concurrentEncryptingThreadsDoesNotInterfere();
    }
}
