package cz.d1x.dxcrypto.encryption;

import cz.d1x.dxcrypto.TestResult;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Tests concurrent encryption whether implementations are thread-safe.
 */
public class ConcurrencyEncryptionTest implements EncryptionTest {

    @Override
    public TestResult test(final EncryptionAlgorithm algorithm) {
        int threads = 1000;
        final AtomicBoolean everythingOk = new AtomicBoolean(true);
        final AtomicInteger finishedThreads = new AtomicInteger(0);

        for (int i = 0; i < threads; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final byte[] origInput = new byte[30];
                        new Random().nextBytes(origInput);
                        byte[] output = algorithm.encrypt(origInput);
                        byte[] input = algorithm.decrypt(output);
                        if (!Arrays.equals(input, origInput)) {
                            everythingOk.set(false);
                        }
                    } catch (Exception ex) {
                        System.out.println("Concurrent encryption fails!");
                        everythingOk.set(false);
                    }
                    finishedThreads.incrementAndGet();
                }
            });
            thread.start();
        }

        while (finishedThreads.get() < threads) {
            try {
                Thread.sleep(100);
                if (!everythingOk.get()) {
                    return TestResult.fail("Any of encryption failed");
                }
            } catch (InterruptedException e) {
                return TestResult.fail("Interrupted thread in test");
            }
        }
        return TestResult.success();
    }
}
