package cz.d1x.dxcrypto;

/**
 * Simple class that contains result of test.
 */
public class TestResult {

    private final boolean success;
    private final String errorMessage;

    private TestResult(boolean success, String errorMessage) {
        this.success = success;
        this.errorMessage = errorMessage;
    }

    public static TestResult success() {
        return new TestResult(true, null);
    }

    public static TestResult fail(String errorMessage) {
        return new TestResult(false, errorMessage);
    }

    public boolean isFailed() {
        return !success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
