package cz.d1x.dxcrypto.encryption.io;

import cz.d1x.dxcrypto.encryption.EncryptionException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Helper class which writes and extracts IV to/from stream
 *
 * @author d.richter
 */
public class IvStreamHelper {

    private final int _blockSize;

    public IvStreamHelper(int blockSize) {
        _blockSize = blockSize;
    }

    public byte[] extractIv(InputStream input) throws EncryptionException {
        try {
            final byte[] iv = new byte[_blockSize];
            final int bytesRead = IOUtils.read(input, iv);
            if (bytesRead < _blockSize) {
                throw new EncryptionException("Wrong data in input stream");
            }
            return iv;
        } catch (IOException e) {
            throw new EncryptionException("Error occurred during iv extraction.", e);
        }
    }

    public void writeIv(OutputStream output, byte[] iv) {
        try {
            output.write(iv);
        } catch (IOException e) {
            throw new EncryptionException("Error occurred during iv write.", e);
        }
    }
}
