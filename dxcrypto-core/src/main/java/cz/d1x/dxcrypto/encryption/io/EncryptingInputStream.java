package cz.d1x.dxcrypto.encryption.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Wrapper around InputStream which reads IV before reading given InputStream
 *
 * @author d.richter
 */
public class EncryptingInputStream extends FilterInputStream {

    private final byte[] iv;
    private AtomicInteger ivReadPosition = new AtomicInteger();

    public EncryptingInputStream(InputStream in, byte[] iv) {
        super(in);
        this.iv = iv;
    }

    @Override
    public int read() throws IOException {
        if (ivReadPosition.get() < iv.length) {
            return iv[ivReadPosition.getAndIncrement()];
        }
        return super.read();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (ivReadPosition.get() < iv.length) {
            final int copyLength = Math.min(len, iv.length - ivReadPosition.get());
            System.arraycopy(iv, ivReadPosition.getAndAdd(copyLength), b, off, copyLength);
            return copyLength;
        }
        return super.read(b, off, len);
    }

    @Override
    public long skip(long n) {
        throw new UnsupportedOperationException("Skip is not supported.");
    }

    @Override
    public int available() throws IOException {
        return (iv.length - ivReadPosition.get()) + super.available();
    }

    @Override
    public synchronized void mark(int readlimit) {
    }

    @Override
    public synchronized void reset() {
    }

    @Override
    public boolean markSupported() {
        return false;
    }
}
